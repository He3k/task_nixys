#! /bin/bash

sudo apt update && sudo apt upgrade -y
sudo apt install mysql-server-8.0 apache2 nginx -y
sudo sed -i "s/80/81/g" /etc/apache2/ports.conf
sudo systemctl restart apache2
