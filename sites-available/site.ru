server {
    listen 80;
    server_name site.ru;

    root /var/www/site.ru;

    index index.php;

    location / {
        try_files $uri $uri/ @backend;
    }

    location /phpmyadmin/ {
       alias /usr/share/phpmyadmin;
    }

    location ~ \.php$ {
                fastcgi_pass 127.0.0.1:9000;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include /etc/nginx/fastcgi_params;
    }

    location ~* .(jpeg|jpg|png|gif|bmp|ico|svg|css|js)$ {
        expires max;
        log_not_found off;
    }

    location @backend {
        proxy_pass http://127.0.0.1:81;
        include proxy_params;
    }
}
