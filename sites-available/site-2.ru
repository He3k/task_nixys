server {
    listen 80;
    server_name site-2.ru;

    root /var/www/site-2.ru;

    index index.php;

    location ~ .php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php8.3-fpm.sock;
    }

    location ~* .(jpeg|jpg|png|gif|bmp|ico|svg|css|js)$ {
        expires max;
        log_not_found off;
    }
}
