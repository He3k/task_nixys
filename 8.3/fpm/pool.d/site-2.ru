[site-2.ru]
user = site-2.ru
group = site-2.ru
listen = /run/php/php8.3-fpm-site-2.ru.sock

; Укажите права доступа к сокету, если используете сокет
listen.owner = www-data
listen.group = www-data
listen.mode = 0660

php_admin_value[disable_functions] = exec,passthru,shell_exec,system
php_admin_flag[allow_url_fopen] = off

