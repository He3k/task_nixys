#! /bin/bash

sudo apt update && sudo apt upgrade -y
dpkg -l | grep php | tee packages.txt
sudo add-apt-repository ppa:ondrej/php
sudo apt update && sudo apt upgrade -y
sudo apt -y install php8.3 php8.3-cli php8.3-fpm php8.3-pdo php8.3-mysql php8.3-zip php8.3-gd php8.3-mbstring php8.3-curl php8.3-xml php-pear php8.3-bcmath